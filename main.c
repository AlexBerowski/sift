/* main.c - SiFT, a simple finance tool */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>

#include "main.h"
#include "new-entry.h"
#include "view.h"

#define SF_OP_INVALID 	0
#define SF_OP_SIFT	1
#define SF_OP_NEWENTRY 	2
#define SF_OP_VIEW 	3

int pick_operation(char *);

int main(int argc, char *argv[])
{
	int retval	= -1;
	int codec_op	= SF_OP_INVALID;

	if ((codec_op = pick_operation(argv[0])) == SF_OP_INVALID) {
		errno = EINVAL;
		perror("sift argv[0] not recognized!");
		exit(-1);
		/* NOTREACHED */
	}

	codec_op = pick_operation(argv[1]);
	switch(codec_op) {
		case SF_OP_NEWENTRY:
			retval = new_entry();
			break;
		case SF_OP_VIEW:
			retval = view();
			break;
		default:
			retval = -1;
			errno = EINVAL;
			fprintf(stderr, "sift: invalid command -- \'%s\'\nTry \'sift --help\' for more information.\n", argv[1]);
	}

	return 0;
}

int pick_operation(char *argv)
{
	char *name;

	if (!argv) {
		errno = EINVAL;
		return SF_OP_INVALID;
	}

	name = basename(argv);

	if (strncmp(name, CMD_SIFT, strlen(CMD_SIFT)) == 0)
		return SF_OP_SIFT;

	if (strncmp(name, CMD_NEWENTRY, strlen(CMD_NEWENTRY)) == 0)
		return SF_OP_NEWENTRY;

	if (strncmp(name, CMD_VIEW, strlen(CMD_VIEW)) == 0)
		return SF_OP_VIEW;

	return SF_OP_INVALID;
}
