/* new-entry.h - SiFT, a simple finance tool */

#ifndef _NEWENTRY_H
#define _NEWENTRY_H

#include <stdio.h>

int new_entry();
// Add function to collect current time.
// Add function to generate SHA256 hash from current time.
void readDate();

#endif /* _NEWENTRY_H */
