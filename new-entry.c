/* new-entry.c - SiFT, a simple finance tool */

#include <stdio.h>
#include "new-entry.h"

int trMonth;
int trDay;
int trYear;

int new_entry()
{
	readDate();
	printf("The transaction occured on: %d/%d/%d\n", trMonth,trDay,trYear);
}

void readDate()
{
	for (;;){
		int daysInMonth[12]={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

		printf("Transaction Date (M/D/YYYY):\n");
		//scanf("%d/%d/%d", &trMonth,&trDay,&trYear);
		char userInput[11];
		fgets(userInput, 11, stdin);

		int parse = sscanf(userInput, "%d/%d/%d", &trMonth,&trDay,&trYear);

		if(trYear % 400 == 0 || (trYear % 100 != 0 && trYear % 4 == 0))
			daysInMonth[1]=29;

		if (trDay < 0 || trDay > daysInMonth[trMonth-1]) {printf("Invalid date: Day does not exist.\n"); continue;}
		if (trMonth < 1 || trMonth > 12) {printf("Invalid date: Month does not exist.\n"); continue;}
		if (trYear < 1900 || trYear > 2099) {printf("Invalid date: Year out of bounds (1900-2099).\n"); continue;}
		break;
	}
}
