# SiFT, a simple finance tool

APP= sift

TARGETS= $(APP)

SRC= main.c new-entry.c view.c
OBJ= $(SRC:.c=.o)
HDR= $(SRC:.c=.h)

MANIFEST= $(SRC) $(HDR) README.md Makefile
VERSION= 0.1.0
SDIST_ROOT= SiFT
SDIST_TARFILE= $(SDIST_ROOT)-$(VERSION).tar.gz

CC= cc
CP= cp
DIFF= diff
LN= ln
MKDIR= mkdir
RM= rm
TAR= tar

CFLAGS= -g
LDFLAGS=

.PHONY: sdist

all: $(TARGETS)

$(APP): $(OBJ)
	$(CC) -o $@ $(OBJ) $(CFLAGS) $(LDFLAGS)

.c.o: $(HDR)
	$(CC) -c $< $(CFLAGS)

sdist: clobber
	$(MKDIR) -p $(SDIST_ROOT)
	$(CP) $(MANIFEST) $(SDIST_ROOT)
	$(TAR) zcf $(SDIST_TARFILE) $(SDIST_ROOT)

README.sift: $(APP) README.md
	./$(APP) < README.md > $@

clean:
	@$(RM) -f $(OBJ) *~ README.sift

clobber: clean
	@$(RM) -f $(TARGETS) $(SDIST_TARFILE)
	@$(RM) -rf $(SDIST_ROOT)
